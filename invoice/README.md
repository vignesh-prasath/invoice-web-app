# Rightstart-Angular
A rightstart kit for Angular projects, and it is preconfigured with <b>Airbnb EsLint Configuration</b>,
<b>Prettier as formatter</b>,
<b>Tailwind CSS as CSS Framework</b>
<b>Husky Pre-commit hook</b> and 
<b>Gherkin for BDD</b>

This repository contains two projects,
#### Seed Project
If you are an expert, we are recommending to go with this project, as it is configured with all necessary standards and doesn't have any dummy code.

This project can be find in the `master`  branch of the [repo](https://bitbucket.org/codaglobal/rightstart-angular/src/master/)

#### Demo Project
Demo project is focused on beginners. It is a basic application built on top of this right-start kit by following all the standards proposed by this initiative.

This project can be find in the `feature/demo` branch of the [repo](https://bitbucket.org/codaglobal/rightstart-angular/src/master/)


## Table of content
1. [Prerequisite](#prerequisite)
2. [Development Server](#development-server)
3. [Production Build](#prod-build)
4. [Test cases](#test-cases)
5. [Contribution](#contribution)
6. [Recommended Documentation](#recommended-documentation)




<a href="prerequisite"></a>

## Prerequisite
#### Install & Setup VSCode
Suggesting to use [VsCode](https://code.visualstudio.com/download) as the Development IDE with following plugins installed


* [Tailwind Intellisense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)

* [Prettier ESLint](https://marketplace.visualstudio.com/items?itemName=rvest.vs-code-prettier-eslint)

* [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
####  Install & Setup NodeJS

 [Download](https://nodejs.org/en/) and install the latest NodeJs (v12+) in the machine. Once completed, run the following command on the terminal to verify the installation. 
 ``` 
    node -v // Node Version
    npm -v // NPM version
 ```

#### Install Angular cli
 ```
 npm install -g @angular/cli

 ```

verify installation by

```
ng --version
```
#### setup development directory

Download this repository to your local machine and unzip the file (Please skip this step incase cloning the repo)
```
cd <download directory>
unzip -a codaglobal-rightstart-angular.zip
cd codaglobal-rightstart-angular
```

Install dependencies using npm

```
npm i
```

<a href="#development-server"></a>
## Development server

Run `npm run start` for a starting the development server and it will open a new default browser window with url `http://localhost:4200`. The app will automatically reload if you change any of the source files.
#### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

<a href="#prod-build"></a>
## Production Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

We recommend to use `npm run build:prod` command for building project for deployment. This command has configured with default linting check on the application, which helps to avoid common issues.

<a href="#test-cases"></a>
## Test cases
#### Running unit tests

Application configured to work with Jasmine & Karma.
Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

The minimum code coverage has been set to 80% globally and component level.

#### Running end-to-end test cases with Gherkin
Application configured to work with [Gherkin](https://cucumber.io/docs/gherkin/reference/) & [Protractor](http://www.protractortest.org/). Which helps to validate the application with customer expectation criteria.

Run `npm run e2e` to execute the end-to-end tests.

Check the Recommended documentation section for further reference on writing gherkin related scenarios


<a href="#contribution"></a>
## Contribution
If you are interested to contribute, please do create a branch under `contribution/<your-branch-name>/seed` &  `contribution/<your-branch-name>/demo` and raise PR to `master` & `feature/demo` branch respectively.



<a href="#recommended-documentation"></a>
## Recommended Documentation

[Tailwind - utility classes](https://tailwindcss.com/docs) <br>
[Gherkin - Write a Scenario](https://cucumber.io/docs/guides/10-minute-tutorial/#write-a-scenario)

