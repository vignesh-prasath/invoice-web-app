import { browser, by, element } from 'protractor';

export class HomePage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  async getTitleText(): Promise<string> {
    return element(by.css('h1.title')).getText() as Promise<string>;
  }
}
