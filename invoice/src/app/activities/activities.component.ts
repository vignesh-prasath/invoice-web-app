import { Component } from '@angular/core';
import { ActivitiesService } from '../services/activitiesService/activities.service';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss'],
})
export class ActivitiesComponent {
  activities: any;

  constructor(public activitiesService: ActivitiesService) {
    this.activitiesService.getActivitiesJSON().subscribe((data) => {
      this.activities = data;
    });
  }
}
