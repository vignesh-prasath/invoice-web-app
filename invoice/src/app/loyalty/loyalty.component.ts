import { Component } from '@angular/core';
import { LoyaltyService } from '../services/loyaltyService/loyalty.service';

@Component({
  selector: 'app-loyalty',
  templateUrl: './loyalty.component.html',
  styleUrls: ['./loyalty.component.scss'],
})
export class LoyaltyComponent {
  loyalty: any;

  constructor(private loyaltyService: LoyaltyService) {
    this.loyaltyService.getLoyaltyJSON().subscribe((data) => {
      console.log(data);
      this.loyalty = data;
    });
  }
}
