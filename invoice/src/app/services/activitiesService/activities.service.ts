import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ActivitiesService {
  constructor(private http: HttpClient) {}

  public getActivitiesJSON() {
    return this.http.get<any>('../assets/Json/activities.json');
  }
}
