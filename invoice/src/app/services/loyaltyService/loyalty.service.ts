import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoyaltyService {
  constructor(private http: HttpClient) {}

  public getLoyaltyJSON() {
    return this.http.get<any>('../assets/Json/loyalty.json');
  }
}
