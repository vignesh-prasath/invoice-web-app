import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InvoiceService {
  constructor(private http: HttpClient) {}

  public getInvoiceJSON() {
    return this.http.get<any>('../assets/Json/invoices.json');
  }
}
