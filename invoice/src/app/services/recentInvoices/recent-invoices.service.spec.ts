import { TestBed } from '@angular/core/testing';

import { RecentInvoicesService } from './recent-invoices.service';

describe('RecentInvoicesService', () => {
  let service: RecentInvoicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecentInvoicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
