import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class RecentInvoicesService {
  constructor(private http: HttpClient) {}

  public getRecentInvoicesJSON() {
    return this.http.get<any>('../assets/Json/recent-invoices.json');
  }
}
