import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TotalRevenueService {
  constructor(private http: HttpClient) {}

  public getTotalRevenueJSON() {
    return this.http.get<any>('../assets/Json/total-revenue.json');
  }
}
