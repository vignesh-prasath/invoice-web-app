import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AnalyticsCardsComponent } from './analytics-cards/analytics-cards.component';
import { TotalRevenueComponent } from './total-revenue/total-revenue.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { ClientsComponent } from './clients/clients.component';
import { LoyaltyComponent } from './loyalty/loyalty.component';
import { MonthlyRevenueComponent } from './monthly-revenue/monthly-revenue.component';
import { NewTemplatesComponent } from './new-templates/new-templates.component';
import { ActivitiesComponent } from './activities/activities.component';
import { RecentInvoicesComponent } from './recent-invoices/recent-invoices.component';
import { SearchComponent } from './search/search.component';
import { ProfileNameComponent } from './profile-name/profile-name.component';
import { InvoiceDashboardComponent } from './invoice-dashboard/invoice-dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SidebarComponent,
    AnalyticsCardsComponent,
    TotalRevenueComponent,
    InvoicesComponent,
    ClientsComponent,
    LoyaltyComponent,
    MonthlyRevenueComponent,
    NewTemplatesComponent,
    ActivitiesComponent,
    RecentInvoicesComponent,
    SearchComponent,
    ProfileNameComponent,
    InvoiceDashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    FormsModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
