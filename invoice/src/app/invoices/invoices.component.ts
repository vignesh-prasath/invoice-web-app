import { Component } from '@angular/core';
import { InvoiceService } from '../services/invoicesService/invoice.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
})
export class InvoicesComponent {
  invoices: any;

  constructor(private invoiceService: InvoiceService) {
    this.invoiceService.getInvoiceJSON().subscribe((data) => {
      console.log(data);
      this.invoices = data;
    });
  }
}
