import { Component } from '@angular/core';
import { single } from '../../assets/bar-data/data';

@Component({
  selector: 'app-monthly-revenue',
  templateUrl: './monthly-revenue.component.html',
  styleUrls: ['./monthly-revenue.component.scss'],
})
export class MonthlyRevenueComponent {
  single: any[];

  multi: any[];

  view: any[] = [850, 150];

  // options
  showXAxis = true;

  showYAxis = false;

  gradient = false;

  showLegend = true;

  showXAxisLabel = false;

  xAxisLabel = 'Country';

  showYAxisLabel = true;

  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#e6e9ef', '#e6e9ef', '#e6e9ef', '#e6e9ef'],
  };

  constructor() {
    Object.assign(this, { single });
  }
}
