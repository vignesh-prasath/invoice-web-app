import { Component } from '@angular/core';
import { RecentInvoicesService } from '../services/recentInvoices/recent-invoices.service';

@Component({
  selector: 'app-recent-invoices',
  templateUrl: './recent-invoices.component.html',
  styleUrls: ['./recent-invoices.component.scss'],
})
export class RecentInvoicesComponent {
  recentInvoice: any;

  constructor(private recentInvoices: RecentInvoicesService) {
    this.recentInvoices.getRecentInvoicesJSON().subscribe((data) => {
      console.log(data, 'text');
      this.recentInvoice = data;
    });
  }
}
