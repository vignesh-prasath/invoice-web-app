import { Component } from '@angular/core';
import { TotalRevenueService } from '../services/totalRevenue/total-revenue.service';

@Component({
  selector: 'app-total-revenue',
  templateUrl: './total-revenue.component.html',
  styleUrls: ['./total-revenue.component.scss'],
})
export class TotalRevenueComponent {
  totalRevenue: any;

  constructor(private totalRevenueService: TotalRevenueService) {
    this.totalRevenueService.getTotalRevenueJSON().subscribe((data) => {
      console.log(data);
      this.totalRevenue = data;
    });
  }
}
