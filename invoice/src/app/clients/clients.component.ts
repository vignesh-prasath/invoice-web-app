import { Component } from '@angular/core';
import { ClientService } from '../services/clientsService/client.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
})
export class ClientsComponent {
  clients: any;

  constructor(private clientService: ClientService) {
    this.clientService.getClientJSON().subscribe((data) => {
      console.log(data);
      this.clients = data;
    });
  }
}
