export const single = [
  {
    name: 'Jan',
    value: 8940000,
  },
  {
    name: 'Feb',
    value: 5000000,
  },
  {
    name: 'Mar',
    value: 7200000,
  },
  {
    name: 'Apr',
    value: 8940000,
  },
  {
    name: 'May',
    value: 5000000,
  },
  {
    name: 'Jun',
    value: 7200000,
  },
  {
    name: 'Jul',
    value: 8940000,
  },
  {
    name: 'Aug',
    value: 5000000,
  },
  {
    name: 'Sep',
    value: 7200000,
  },
  {
    name: 'Oct',
    value: 8940000,
  },
  {
    name: 'Nov',
    value: 5000000,
  },
  {
    name: 'Dec',
    value: 7200000,
  },
];
